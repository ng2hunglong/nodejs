const path = require('path');
// let pathInfo = {
//   baseName: path.basename()
// }


/*
  // console.log('foo/bar/baz'.split('/'));
  // the same as:
    // On Linux:
      // console.log('foo/bar/baz'.split(path.sep));

    // On Windows:
      // console.log('foo\\bar\\baz'.split(path.sep));
*/
let filePath = path.join('/public', 'img', 'cool.png');
console.log(filePath);
let baseName = path.basename(filePath);
console.log(baseName);

let absolutePath = path.resolve(__dirname, 'public', 'js', 'main.js');
console.log(absolutePath);