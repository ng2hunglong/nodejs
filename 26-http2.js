const http2 = require('http2');
const fs = require('fs');

const server = http2.createSecureServer({
  key: fs.readFileSync('./localhost-privkey.pem'),
  cert: fs.readFileSync('./localhost-cert.pem'),
  allowHTTP1: true,
});
server.on('error', err => console.log(err));
server.on('stream', (stream, headers) => {
  stream.respond()
})