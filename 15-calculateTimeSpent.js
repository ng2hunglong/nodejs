const doSomething = () => console.log('test');
const measureDOingSomething = () => {
  console.time('doSomething()')
  doSomething();
  console.timeEnd('doSomething()')
}

measureDOingSomething()
console.log('\x1b[33m%s\x1b[0m', 'hi!')