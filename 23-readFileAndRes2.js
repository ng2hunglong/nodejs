const { createReadStream } = require('fs')
const http = require('http')

const server = http.createServer((req, res) => {
  res.status = 200;
  res.setHeader('Content-Type', 'text/html');
  const fileStream = createReadStream('./content/bigFile.txt', 'utf-8');
  fileStream.on('open', () => { // read stream
    // fileSteam.pipe method will push read stream into write stream
    // if we can read data in chunk, we can write data in chunk.
    
    fileStream.pipe(res)
  })
  fileStream.on('error', (err) => {
    res.end(err);
  })
})

server.listen(5050, () => {
  console.log('Server is running at port 5050');
})