const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send("Hi from Express!");
})

app.listen(3000, () => {console.log("Server's readly")});

// on SIGTERM signal, close server and console.log
process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Process terminated')
  })
})

setTimeout(() => {
  process.kill(process.pid, 'SIGTERM');
}, 5000);