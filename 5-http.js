const http = require('http');

const server = http.createServer((req, res) => {
  // console.log(req);
  if (req.url === '/') {
    res.end('Welcome to our home page');
  }
  if (req.url === '/about') {
    res.end('Author: Nguyen Ngoc Hung Long');
  }
  res.write('Welcome to our home page2');
  res.end(`
    <h1>Oops!</h1>
    <p>Your url is invalid.</p>
  `);
})

server.listen(5000);