const { createReadStream } = require('fs')

const stream = createReadStream('./content/bigFile.txt', { highWaterMark: 9000})

stream.on('data', (result) => {
  console.log(result);
})

// default 64kb
// last buffer - remainder
// highWaterMark - control size
// const stream = createReaStream('./content/bigFile.txt', {highWaterMark: 9000})
// const stream = createReaStream('./content/bigFile.txt', { encoding: 'utf-8'})