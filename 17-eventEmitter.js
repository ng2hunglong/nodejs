const EventEmitter = require('events')
const eventEmitter = new EventEmitter()

eventEmitter.on('start', (number) => {
  console.log('started with number ' + number);
})
eventEmitter.emit('start', 25);