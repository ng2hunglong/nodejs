const { readFileSync, writeFileSync } = require('fs')

const first = readFileSync('./content/subfolder/first.txt', 'utf8');
const second = readFileSync('./content/second.txt', 'utf8');

console.log(first, second);

// create a file:
  // writeFileSync('./content/third.txt', `I created third.txt by reading first.txt: ${first}`)

// append a file:
writeFileSync(
  './content/third.txt',
  '\nI appended this line.',
  {flag: 'a'}
);