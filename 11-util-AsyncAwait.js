const { readFile, writeFile } = require('fs');
const util = require('util');
const readFilePromise = util.promisify(readFile);
const writeFilePromise = util.promisify(writeFile);

const start = async () => {
  try {
    const first = await readFilePromise('./content/subfolder/first.txt', 'utf8');
    const second = await readFilePromise('./content/second.txt', 'utf8');
    console.log(first, second);
    await writeFilePromise(
      './content/third.txt',`I used util library of Node js to write this File.\nAhihi do ngoc!!!`);
  } catch (err) {
    console.log(err);
  }
}
start();