const { readFile, writeFile } = require('fs');

readFile('./content/subfolder/first.txt', 'utf8', (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  let firstRead = result;
  readFile('./content/second.txt', 'utf8', (err, result) => {
    if (err) {
      console.log(err);
      return;
    }
    let secondRead = result;
    console.log(firstRead, secondRead);
    writeFile(
      './content/third.txt',
      '\nI write this using callback of synchronous function',
      {flag: 'a'}
      ,(err, result) => {
        if (err) {
          console.log(err);
          return;
        }
        console.log(result);
      }
    )
  })
})