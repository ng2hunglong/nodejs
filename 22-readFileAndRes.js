const { readFileSync } = require('fs')
const http = require('http')



const server = http.createServer((req, res) => {
  res.status = 200;
  res.setHeader('Content-Type', 'text/html');
  const text = readFileSync('./content/bigFile.txt');
  res.end(text);
})

const port = 5000;

server.listen(port, () => {
  console.log(`Server is running at port ${port}`);
})