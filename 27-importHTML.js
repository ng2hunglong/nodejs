const http = require('http')

const { readFileSync } = require('fs')

const homePage = readFileSync('./homePage.html')

const server = http.createServer((req, res) => {
  if (req.url === '/' || req.url === '/home') {
    res.writeHead(200, {'content-type': 'text/html'})
    res.write(homePage)
    res.end()
  } else {
    res.writeHead(400)
    res.end()
  }
})
server.listen(5000, () => console.log(`Server is running at port 5000`))