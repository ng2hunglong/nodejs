// console.log(__dirname);
// console.log(__filename);

// console.log(module); // - info about current module (file)
// {
//   id: '.',
//   path: '...',
//   exports: {},
//   parent: null,
//   filename: '...',
//   loaded: false,
//   children: [],
//   paths: [
//     '...'
//     '...'
//     '...'
//     '...'
//     '...'
//   ]
// }

// console.log(process); // info about env the program is being executed

// node have built in function like: setInterval and setTimeout
setInterval(() => {
  console.log("hello world!");
},1000)