const { writeFileSync } = require('fs')

let text = '';
for (let i = 0; i < 100000; i++) {
  text += `Hello Big File ${i}\n`;
}
writeFileSync(
  './content/bigFile.txt',
  text,
  { flag: 'w'}
);