const { readFile, writeFile } = require('fs');

async function loadJson() {
  try {
    let file1 = await getText('./content/subfolder/first.txt');
    console.log(file1);
    let file2 = await getText('./content/second.txt');
    console.log(file2);
  } catch (err) {
    console.log(err);
  }
}

function getText(url) {
  return new Promise((resolve, reject) => {
    readFile(url, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    })
  })
}

loadJson();
